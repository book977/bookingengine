import { Injectable, EventEmitter } from '@angular/core';
import { config } from '../config/config'
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class CountryService {
    COUNTRY_URL = 'assets/countries.json'
    constructor(private http: Http) { }

    getListOfCountries() {

        return this.http
            .get(this.COUNTRY_URL)
            .map((response: Response) => response.json())
    }

}
