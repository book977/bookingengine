import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RoutingModule } from './config/routing/routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HomeComponent } from './dashboard/home/home.component';

import { AuthGuard } from './shared/auth-guard';
import { RegisterComponent } from './register/register.component';
import { PropertiesComponent } from './properties/properties.component';
import { AddpropertiesComponent } from './properties/addproperties/addproperties.component';
import { ResourcesComponent } from './resources/resources.component';
import { AddresourcesComponent } from './resources/addresources/addresources.component';
import { AddpropertiestypeComponent } from './properties/addpropertiestype/addpropertiestype.component';
import { ViewpropertiestypesComponent } from './properties/viewpropertiestypes/viewpropertiestypes.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    DashboardComponent,
    HomeComponent,
    PropertiesComponent,
    AddpropertiesComponent,
    ResourcesComponent,
    AddresourcesComponent,
    AddpropertiestypeComponent,
    ViewpropertiestypesComponent],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RoutingModule
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
