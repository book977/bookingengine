import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { CountryService } from '../shared/country.service';
import { RegisterService } from './register.service'
import { Router } from '@angular/router';
import { Response } from '@angular/http';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [CountryService, RegisterService]
})
export class RegisterComponent implements OnInit {
  public userForm: FormGroup;
  countries: any;
  public message: any;
  public inProgress: boolean = false;

  constructor(private _fb: FormBuilder
    , private countryService: CountryService
    , private router: Router
    , private registerService: RegisterService) { }

  ngOnInit() {
    this.clearStorage();
    this.userForm = this._fb.group({
      company_name: new FormControl('', [<any>Validators.required]),
      email: new FormControl('', [<any>Validators.required]),
      password: new FormControl('', [<any>Validators.required]),
    });

    // this.getCountries()
  }

  clearStorage() {
    localStorage.clear();
  }

  getCountries() {
    this.countryService.getListOfCountries()
      .subscribe(
      data => {
        this.countries = data;
      });
  }

  register() {
    this.inProgress = true;
    this.message = null;     
    this.registerService.register(this.userForm.value).subscribe((authRes: Response) => {
      this.inProgress = false;
      this.router.navigate(['/login'])
    }, (error: Response) => {
      this.inProgress = false;
      var message = error.json();
      this.message = "* " + message.message;
    })
  }

  navigateToLogin() {
    this.router.navigate(['/login'])
  }

}
