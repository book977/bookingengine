import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import 'rxjs/add/observable/of';
import { config } from '../config/config'

@Injectable()
export class RegisterService {
    private loggedIn = false;
    BASE_URL = config.API_BASE_URL;
    AUTH_URL = this.BASE_URL + 'register';

    constructor(private http: Http) { }

    register(userCred) {
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http
            .post(this.AUTH_URL, userCred, { headers: headers })
            .map((response: Response) => {

                let user = response.json();
                if (user.status != "error") {
                    if (user && user.payload.token) {
                        this.loggedIn = true;
                        localStorage.setItem('authorization', user.payload.token);
                    }
                }
                return response;
            });
    }
}