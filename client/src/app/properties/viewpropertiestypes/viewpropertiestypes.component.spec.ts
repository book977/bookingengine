import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewpropertiestypesComponent } from './viewpropertiestypes.component';

describe('ViewpropertiestypesComponent', () => {
  let component: ViewpropertiestypesComponent;
  let fixture: ComponentFixture<ViewpropertiestypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewpropertiestypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewpropertiestypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
