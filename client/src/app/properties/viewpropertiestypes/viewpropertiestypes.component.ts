import { Component, OnInit } from '@angular/core';
import { PropertiesService } from '../properties.service'

@Component({
  selector: 'app-viewpropertiestypes',
  templateUrl: './viewpropertiestypes.component.html',
  styleUrls: ['./viewpropertiestypes.component.css'],
  providers: [PropertiesService]
})
export class ViewpropertiestypesComponent implements OnInit {
  responseMessage:any;
  propertiesTypes:any;
  hasPropertyTypes:boolean;

  constructor( private propService: PropertiesService) { }

  ngOnInit() {
    this.getPropertyTypes();
  }


  getPropertyTypes() {
    this.propService.getPropertyType().subscribe((propRes) => {
     this.propertiesTypes = propRes.payload.property_type;
     if(this.propertiesTypes.length>0) {
       this.hasPropertyTypes = true;
     } else {
       this.hasPropertyTypes = false;
     }

    },(error: Response) => {
      var message = error.json();
      this.responseMessage = "* " + message
    })
  }

}
