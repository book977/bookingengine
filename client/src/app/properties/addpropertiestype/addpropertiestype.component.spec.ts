import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddpropertiestypeComponent } from './addpropertiestype.component';

describe('AddpropertiestypeComponent', () => {
  let component: AddpropertiestypeComponent;
  let fixture: ComponentFixture<AddpropertiestypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddpropertiestypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddpropertiestypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
