import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { PropertiesService } from '../properties.service'
import { Response } from '@angular/http';
import { Router } from '@angular/router';
@Component({
  selector: 'app-addpropertiestype',
  templateUrl: './addpropertiestype.component.html',
  styleUrls: ['./addpropertiestype.component.css'],
  providers: [PropertiesService]
})
export class AddpropertiestypeComponent implements OnInit {
  propertyTypeForm: FormGroup;
  message: any;
  constructor(private fb: FormBuilder
    , private propService: PropertiesService
    , private router: Router) { }

  ngOnInit() {
    this.initiatePropertyForm();
  }

  initiatePropertyForm() {
    this.propertyTypeForm = this.fb.group({
      propertyType: this.fb.array([
        this.initPropertyType()
      ])

    });
  }

  initAddress() {
    return this.fb.group({
      street: [''],
      city: [''],
      state: [''],
      zip: ['']
    });
  }

  initResources() {
    return this.fb.group({
      name: [''],
      description: [''],
      extra_price: [''],
      max_quantity: ['']
    });
  }

  initPropertyType() {
    return this.fb.group({
      name: [''],
      address: [''],
      price: [''],
      is_cancelable: [true],
      max_no_of_guest: [1]
    });
  }

  addPropertyType(submitAndContinue) {
    var propertyData = this.propertyTypeForm.value;

    this.propService.addPropertyType(propertyData.propertyType).subscribe((propRes) => {
      if (submitAndContinue == false) {
        this.router.navigate(['/propertiesTypes'])
      } else {
        this.propertyTypeForm.reset()
      }

    }, (error: Response) => {
      var message = error.json();
      this.message = "* " + message.message;
    })
  }
}
