
import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import 'rxjs/add/observable/of';
import { URLS } from '../config/api/urls'

@Injectable()
export class PropertiesService {
    companyApi = URLS.companies;
    sessionInfo = localStorage.getItem('authorization');

    constructor(private http: Http) { }

    addProperty(propertyData) {
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http
            .post(this.companyApi, propertyData)
            .map((response: Response) => {
                return response.json();
            });
    }

    addPropertyType(propertyData) {
        var token = localStorage.getItem('authorization');
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('x-access-token', token);
        return this.http
            .post(this.companyApi+'/propertyType', propertyData,{headers})
            .map((response: Response) => {
                return response.json();
            });
    }

    getPropertyType() {
        var token = localStorage.getItem('authorization');
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('x-access-token', token);
        return this.http
            .get(this.companyApi+'/propertyType', {headers})
            .map((response: Response) => {
                return response.json();
            });
    }
}