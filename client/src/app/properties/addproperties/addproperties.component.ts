import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { PropertiesService } from '../properties.service'

@Component({
  selector: 'app-addproperties',
  templateUrl: './addproperties.component.html',
  styleUrls: ['./addproperties.component.css'],
  providers: [PropertiesService]
})
export class AddpropertiesComponent implements OnInit {
  propertyTypeForm: FormGroup;

  constructor(private fb: FormBuilder
    , private propService: PropertiesService) { }

  ngOnInit() {
    this.initiatePropertyForm();
  }

  initiatePropertyForm() {
    this.propertyTypeForm = this.fb.group({
      propertyType: this.fb.array([
        this.initPropertyType()
      ])

    });
  }

  initAddress() {
    return this.fb.group({
      street: [''],
      city: [''],
      state: [''],
      zip: ['']
    });
  }

  initResources() {
    return this.fb.group({
      name: [''],
      description: [''],
      extra_price: [''],
      max_quantity: ['']
    });
  }

  initPropertyType() {
    return this.fb.group({
      name: [''],
      address: [''],
      price: [''],
      cancellable: [true],
      max_no_guest: [1]
    });
  }

  addProperty() {
    var propertyData = this.propertyTypeForm.value;
    console.log(propertyData.propertyType)
    this.propService.addProperty(propertyData.propertyType).subscribe((propRes) => {

    }, error => {
      console.log(error);
    })
  }

}
