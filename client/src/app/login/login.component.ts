import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from './login.service'
import { Response } from '@angular/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginService]
})
export class LoginComponent implements OnInit {
  public userForm: FormGroup;
  public message: any;
  public inProgress: boolean = false;

  constructor(private _fb: FormBuilder
    , private loginService: LoginService
    , private router: Router) {

  }

  ngOnInit() {
    this.clearStorage();
    this.userForm = this._fb.group({
      email: new FormControl('', [<any>Validators.required]),
      password: new FormControl('', [<any>Validators.required]),
    });
  }

  clearStorage() {
    localStorage.clear();
  }

  login() {
    this.inProgress = true;
    this.message = null;
    this.loginService.login(this.userForm.value).subscribe((authRes: Response) => {
      this.router.navigate(['/home']);
      this.inProgress = false;
    }, (error: Response) => {
      this.inProgress = false;
      var message = error.json();
      console.log(message)
      this.message = "* " + message.message;
    })
  }

  navigateToRegister() {
    this.router.navigate(['/register'])
  }

}
