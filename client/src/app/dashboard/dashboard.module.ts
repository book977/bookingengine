import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoutingModule } from '../config/routing/routing.module';

import { DashboardComponent } from './dashboard.component';
import { HomeComponent } from './home/home.component';

@NgModule({
  imports: [
    CommonModule,
    RoutingModule
  ],
  declarations: [DashboardComponent, HomeComponent]
})
export class DashboardModule { }
