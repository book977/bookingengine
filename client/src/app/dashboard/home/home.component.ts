import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  value: any;

  constructor(private routes: Router) { }

  ngOnInit() {
    
  }

  routeToAddProperties() {
    this.value = 1;
    this.routes.navigate(['/home', { outlets: { 'dashboardOutlet': [this.value] } }]);
  }

}
