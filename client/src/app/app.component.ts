import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import 'rxjs/add/operator/filter'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app works!';
  layout: any;

  constructor(private _router: Router, private route: ActivatedRoute) {


  }

  ngOnInit() {
    this.getLayoutType();
  }

  getLayoutType() {
    this._router.events
      .filter(e => e instanceof NavigationEnd)
      .forEach(e => {
        this.layout = this.route.root.firstChild.snapshot.data['layout'];
      });
  }

  logout() {
    this._router.navigate(['/login']);
  }

  viewProperties() {
    this._router.navigate(['/properties']);
  }

  viewPropertiesTypes() {
    this._router.navigate(['/propertiesTypes']);
  }

  routeToAddPropertiesType() {
    this._router.navigate(['/propertiestype/add']);
  }

  viewResources() {
    this._router.navigate(['/resources']);
  }

  addResources() {
    this._router.navigate(['/resources/add']);
  }


}
