import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { modulesRouting } from './routing.interface';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(modulesRouting, { useHash: true })
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class RoutingModule { }
