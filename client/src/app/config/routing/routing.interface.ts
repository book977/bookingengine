import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from '../../login/login.component'
import { RegisterComponent } from '../../register/register.component'
import { HomeComponent } from '../../dashboard/home/home.component'
// properties
import { PropertiesComponent } from '../../properties/properties.component'
import { AddpropertiesComponent } from '../../properties/addproperties/addproperties.component'
import { AddpropertiestypeComponent } from '../../properties/addpropertiestype/addpropertiestype.component'
import { ViewpropertiestypesComponent } from '../../properties/viewpropertiestypes/viewpropertiestypes.component'



import { ResourcesComponent } from '../../resources/resources.component'
import { AddresourcesComponent } from '../../resources/addresources/addresources.component'
import { AuthGuard } from '../../shared/auth-guard';

export const modulesRouting: Routes = [
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    { path: 'login', component: LoginComponent, data: { layout: 'default' } },
    { path: 'register', component: RegisterComponent, data: { layout: 'default' } },
    { path: 'home', component: HomeComponent, data: { layout: 'dashboard' }, canActivate:[AuthGuard] },
    { path: 'properties', component: PropertiesComponent, data: { layout: 'dashboard' }, canActivate:[AuthGuard] },
    { path: 'properties/add', component: AddpropertiesComponent, data: { layout: 'dashboard' }, canActivate:[AuthGuard] },
    { path: 'propertiestype/add', component: AddpropertiestypeComponent, data: { layout: 'dashboard' }, canActivate:[AuthGuard] },
    { path: 'propertiesTypes', component: ViewpropertiestypesComponent, data: { layout: 'dashboard' }, canActivate:[AuthGuard] },
    { path: 'resources', component: ResourcesComponent, data: { layout: 'dashboard' }, canActivate:[AuthGuard] },
    { path: 'resources/add', component: AddresourcesComponent, data: { layout: 'dashboard' }, canActivate:[AuthGuard] },
];
