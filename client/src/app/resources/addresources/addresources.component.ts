import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { ResourcesService } from '../resources.service'

@Component({
  selector: 'app-addresources',
  templateUrl: './addresources.component.html',
  styleUrls: ['./addresources.component.css'],
  providers: [ResourcesService]
})
export class AddresourcesComponent implements OnInit {
  resourceForm: FormGroup;

  constructor(private fb: FormBuilder
    , private resourcesService: ResourcesService) { }

  ngOnInit() {
    this.initiateResourcesForm();
  }

  initiateResourcesForm() {
    this.resourceForm = this.fb.group({
      resources: this.fb.array([
        this.initResources()
      ])

    });
  }

  initResources() {
    return this.fb.group({
      name: [''],
      desc: [''],
      extra_price: [''],
      MaxQty: ['']
    });
  }

  addResources() {

    this.resourcesService.addResource(this.resourceForm.value, '232131231').subscribe((resResponse) => {
      console.log(resResponse)
    }, error => {
      console.log(error);
    })
  }

}
