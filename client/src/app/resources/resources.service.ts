
import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import 'rxjs/add/observable/of';
import { URLS } from '../config/api/urls'

@Injectable()
export class ResourcesService {
    companyApi = URLS.companies;

    constructor(private http: Http) { }

    addResource(resourcesData, companyId) {
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http
            .post(this.companyApi, resourcesData)
            .map((response: Response) => {
                return response.json();
            });
    }
}