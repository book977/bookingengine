import { EasybookingPage } from './app.po';

describe('easybooking App', () => {
  let page: EasybookingPage;

  beforeEach(() => {
    page = new EasybookingPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
