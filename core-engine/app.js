'use strict';

process.env.NODE_ENV = process.env.NODE_ENV || 'development';
const path = require('path');
process.env.PWD = process.env.PWD || path.resolve(__dirname);


const cors = require('cors');
const envConfig = require(`${process.env.PWD}/config/env/envConf`);

const express = require('express');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
const port = process.env.PORT || envConfig.get('PORT');

const db = require(`./src/utils/DbConnection`).db.connection;

const CustomError = require(`./helpers/customError`);

var app = express();
app.use(cors());
app.options('*', cors());

// Generates API docs using Swagger
require(`./helpers/swagger`).genDoc(app);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//check if the mongodb is connected
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log('mongodb server has been connected and now open.');
});



app.get('/', function (req, res, next) {
    res.render('index', {
        title: 'yaybookingapi'
    });
});


require('./helpers/authMiddleware').route(app);

// Include all routes
require(`./helpers/routes`).route(app);

// Authorize routes

// Create Bad Request error if URI is not found
app.all('*', function (req, res, next) {
    next(new CustomError.BadRequest());
});

//Handle errors with json response
app.use(require(`${process.env.PWD}/helpers/errorHandler`));

var timeout = require('connect-timeout'); //express v4

app.use(timeout(1200000));
app.use(haltOnTimedout);

function haltOnTimedout(req, res, next) {
    if (!req.timedout) next();
}

const server = app.listen(port, () => {
    console.log(`yaybook server is running at port ${port}`);
});

server.timeout = 240000;

module.exports = app;