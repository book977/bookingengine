'use strict';

const Promise = require('bluebird');

const repository = require('../../models/repository');
const Property = require('../../models/property.model');


/**
 * list a particular property
 * @param availableParams {Object}
 *  eg:
 *  {
 *   limit: 10,
 *   offset: 10,
 *   propertyName: 'deluxe'
 *  }
 * @return Promise
 *  resolve{Property[]}
 *  reject{Error}
 */
function checkParticularAvailable(req) {
  return new Promise(function (resolve, reject) {
    var propertyId = req.params.propertyId;
    Property.findById(propertyId,
      function (err, doc) {
        if (err) {
          // If it failed, return error
          reject('property not available');
        } else {
          // And forward to success page
          resolve(doc);
        }
      });
  });

}


/**
 * list all properties
 * @param availableParams {Object}
 *  eg:
 *  {
 *   limit: 10,
 *   offset: 10,
 *   propertyName: 'deluxe'
 *  }
 * @return Promise
 *  resolve{Property[]}
 *  reject{Error}
 */
function checkAvailable(req) {
  return new Promise(function (resolve, reject) {
    Property.find({}, function (err, property) {
      if (err)
        reject(err);
      resolve(property);
    });
  });
}



module.exports = {
  checkAvailable,
  checkParticularAvailable
};