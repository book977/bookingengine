'use strict';

const Promise = require('bluebird');

const repository = require('../../models/repository');
const Reservation = require('../../models/reservations.model');
const Reserved = require('../../models/reserved.model');


/**
 * create reservations and reserved
 * @param propertyParams {Object}
 *  eg:
 *  {
 *   limit: 10,
 *   offset: 10,
 *   propertyName: 'deluxe'
 *  }
 * @return Promise
 *  resolve{reservation[]}
 *  reject{Error}
 */
function bookParticularProperty(req) {
  reservationModelObj = new Reservation(req.body);
  reservedModelObj = new Reserved(req.body);
  return new Promise(function (resolve, reject) {
    reservationModelObj.save((function (err, data) {
      if (err) {
        reject(err);
      } else {
        reservedModelObj.save(function (err, data) {
          if (err)
            reject(err);
          resolve(data);
        })
      }
    }))

  });
}



module.exports = {
  bookParticularProperty
};