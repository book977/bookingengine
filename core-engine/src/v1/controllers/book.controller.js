'use strict';

const BookService = require('../../services/dataAccess/book.service');

function bookProperty(req, res, next) {
  BookService.bookParticularProperty(req)
    .then(response => {
      res.send(response);
    })
    .catch(err => {
      next(err);
    })
}

module.exports = {
  bookParticularProperty,
};