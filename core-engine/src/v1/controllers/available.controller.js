'use strict';

const AvailableService = require('../../services/dataAccess/available.service');
const Property = require('../../models/property.model');


function checkParticularAvailable(req, res, next) {
  AvailableService.checkParticularAvailable(req)
    .then(response => {
      res.send(response);
    })
    .catch(err => {
      next(err);
    })
}

function checkAvailable(req, res, next) {
  AvailableService.checkAvailable(req)
    .then(response => {
      res.send(response);
    })
    .catch(err => {
      next(err);
    })
}



module.exports = {
  checkParticularAvailable,
  checkAvailable,
};