'use strict';

const express = require('express'),
  router = express.Router();

const availableController = require('../controllers/available.controller');


/**
 * @swagger
 * /v1/checkAvailability/{propertyId}:
 *    get:
 *      tags:
 *        - Properties
 *      produces:
 *        - application/json
 *      description: List a particular available property
 *      parameters:
 *        - in: header
 *          required: true
 *          name: x-access-token
 *          type: string
 *        - in: path
 *          name: propertyId
 *          type: number
 *          describe: id of property to be listed
 *      responses:
 *        "200":
 *          description: Success
 *          schema:
 *            type: array
 *            items:
 *              $ref: "#/definitions/Properties"
 *        default:
 *          description: Error
 *          schema:
 *            $ref: "#/definitions/ErrorResponse"
 */
router.get('/checkAvailability/:propertyId', availableController.checkParticularAvailable);

/**
 * @swagger
 * /v1/checkAvailability:
 *    get:
 *      tags:
 *        - Properties
 *      produces:
 *        - application/json
 *      description: List all available properties
 *      parameters:
 *        - in: header
 *          required: true
 *          name: x-access-token
 *          type: string
 *      responses:
 *        "200":
 *          description: Success
 *          schema:
 *            type: array
 *            items:
 *              $ref: "#/definitions/Properties"
 *        default:
 *          description: Error
 *          schema:
 *            $ref: "#/definitions/ErrorResponse"
 */
router.get('/checkAvailability', availableController.checkAvailable);


module.exports = router;