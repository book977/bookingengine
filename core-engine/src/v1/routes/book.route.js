'use strict';

const express = require('express'),
  router = express.Router();

const bookController = require('../controllers/book.controller');


/**
 * @swagger
 * /v1/bookProperty:
 *    put:
 *      tags:
 *        - Properties
 *      produces:
 *        - application/json
 *      description: book a particular property
 *      parameters:
 *        - in: header
 *          required: true
 *          name: x-access-token
 *          type: string
 *        - in: path
 *          name: propertyId
 *          type: number
 *          describe: id of property to be booked
 *      responses:
 *        "200":
 *          description: Success
 *          schema:
 *            type: array
 *            items:
 *              $ref: "#/definitions/Properties"
 *        default:
 *          description: Error
 *          schema:
 *            $ref: "#/definitions/ErrorResponse"
 */
router.put('/bookProperty', bookController.bookProperty);


module.exports = router;