'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ReservationsSchema = new Schema({
  _CID: { type: Number, required: true, unique: true },
  company: {
    _CID: { type: String, required: true },
    name: { type: String, required: true },
    address: { type: String, required: true }
  },
  property_type: {
    name: { type: String, required: true },
    address: { type: String, required: true },
    price: { type: String, required: true },
    is_cancelable: { type: Boolean, default: true },
    max_no_of_guest: { type: Number, required: true }
  },
  no_of_properties: { type: Number, required: true },
  properties: [{
    property_name: { type: String, required: true },
    property_type: { type: String, required: true }
  }],
  start_date: { type: Date },
  end_date: { type: Date },
  reserved_date_time: { type: Date, required: true },
  total_price: { type: String },
  Resources: [{
    name: { type: String, required: true },
    desc: { type: String, required: true },
    extra_price: { type: String, required: true },
    MaxQty: { type: String },
  }],
  guest: {
    name: { type: String },
    address: { type: String },
    email: { type: String }
  },
  is_cancelable: { type: Boolean, default: true },
  no_of_adults: { type: String },
  no_of_infants: { type: String },
  status: { type: String }
});


var Reservation = mongoose.model('Reservations', ReservationsSchema);

module.exports = Reservation;