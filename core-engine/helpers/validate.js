function validateDomain(domain) {
  if (/^[a-zA-Z0-9\-_]{1,20}$/.test(domain)) {
    console.log('Input is alphanumeric');
    return true;
  }
  return false;
}

function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}


module.exports = {
  validateDomain,
  validateEmail
};