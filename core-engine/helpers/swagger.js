'use strict';

const express = require('express');
const path = require('path');
const swaggerJSDoc = require('swagger-jsdoc');

module.exports = {
  genDoc
};
const swaggerOption = {
  swaggerDefinition: {
    info: {
      title: 'Spirathon',
      version: '1.0.0',
      description: 'Public API for Spirathon'
    },
  },
  apis: [path.join(__dirname, './authMiddleware.js'), path.join(__dirname, '../src/**/routes/*.js'), path.join(__dirname, '../swagger/**.yaml')]
};

function genDoc(app) {
  // initialize swagger-jsdoc
  const swaggerSpec = swaggerJSDoc(swaggerOption);

  app.use(express.static(path.join(__dirname, '../public')));

  app.get('/swagger.json', function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.send(swaggerSpec);
  });
}