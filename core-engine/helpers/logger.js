'use strict';

const winston = require('winston'),
  expressWinston = require('express-winston');

module.exports = {
  log,
  errorLog
};

const options = {
  transports: [
    new winston.transports.Console({
      json: true,
      colorize: true
    })
  ]
};

function log() {
  return expressWinston.logger(options);
}

function errorLog() {
  return expressWinston.errorLogger(options);
}


