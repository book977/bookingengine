var express = require('express');
var app = express();
var cors = require('cors')
var bodyParser = require('body-parser');
var common = require('./api/config/common')
var config = common.config();
app.use(cors());

var bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json());

var port = process.env.PORT || 8080;

// var router = require('./api/routes/routes');
var router = require(`./api/helpers/routes`).route(app);

// more routes for our API will happen here

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
// app.use('/api', router);

// db config
var mongoose = require('mongoose');
mongoose.connect(config.DBENV.DB);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Server started on port  ' + port);