var express = require('express');
var router = express.Router();
var PropertiesController = require('../controllers/propertiesController');

router.post('/properties', PropertiesController.create)
router.post('/properties/:propertyId', PropertiesController.update)

module.exports = [router]