var express = require('express');
var router = express.Router();
var common = require('../config/common')
var config = common.config();

var UserController = require('../controllers/user');
var jwt = require('jsonwebtoken');

router.post('/auth', UserController.authenticate);
router.post('/register', UserController.create)

router.use(function (req, res, next) {
    var token = req.body.token || req.query.token || req.headers['x-access-token'] || req.body.headers['x-access-token'][0];
    if (token) {
        jwt.verify(token, config.SUPER_SECRET_FOR_TOKEN, function (err, decoded) {
            if (err) {
                return res.status(401).json({ status: "error", message: 'Failed to authenticate token.' });
            } else {
                // if everything is good, save to request for use in other routes
                req.decoded = decoded;
                next();
            }
        });

    } else {
        return res.status(403).send({
            status: "error",
            message: 'No token provided.'
        });

    }
})

module.exports = [router]