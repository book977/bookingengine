var express = require('express');
var router = express.Router();
var CompaniesController = require('../controllers/companiesController');

router.post('/companies', CompaniesController.create)
router.put('/companies/:companyId', CompaniesController.update)
router.post('/companies/propertyType', CompaniesController.createPropertyType)
router.get('/companies/propertyType', CompaniesController.getPropertyType)

module.exports = [router]