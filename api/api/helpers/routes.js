'use strict';

const directory = require('require-dir');

module.exports = {
    route
};

function route(app) {
    var routes = directory('../routes');
    
    for (var i in routes) app.use('/api', routes[i]);
}