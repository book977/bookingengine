'use strict';
var UserModel = require("../models/user");

function create(req, res, next) {
    UserModel.create(req.body).then(function (userCreateRes) {
        res.status(201).send({ status: "success", "payload": userCreateRes, message:"Save token for future requests." })
    }, error => {
        res.status(400).send({ status: "error", "message": error })
    })
}

function authenticate(req, res, next) {
    UserModel.getAuth(req.body).then(function (authRes) {
    res.status(200).send({ status: "success", "payload": authRes,message:"Save token for future requests."  })
  }).catch(function (error) {
     res.status(400).send({ status: "error", "message": error })
  })
}


module.exports = {
    create,
    authenticate
}