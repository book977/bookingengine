'use strict';
var PropertiesModel = require("../models/propertiesModel");

function create(req, res, next) {
    PropertiesModel.create(req.body).then(function (propertyCreateRes) {
        res.status(201).send({ status: "success", "payload": propertyCreateRes, message: "Properties saved successfully." })
    }, error => {
        res.status(400).send({ status: "error", "message": error })
    })
}

function update(req, res, next) {
    PropertiesModel.update(req.body, req.params.propertyId).then(function (propertyUpdateRes) {
        res.status(201).send({ status: "success", "payload": propertyUpdateRes, message: "Companies saved successfully." })
    }, error => {
        res.status(400).send({ status: "error", "message": error })
    })
}

module.exports = {
    create,
    update
}