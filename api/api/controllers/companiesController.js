'use strict';
var CompaniesModel = require("../models/companiesModel");

function create(req, res, next) {
  CompaniesModel.create(req.body).then(function (companyCreateRes) {
    res.status(201).send({ status: "success", "payload": companyCreateRes, message: "Companies saved successfully." })
  }, error => {
    res.status(400).send({ status: "error", "message": error })
  })
}

function update(req, res, next) {
  CompaniesModel.update(req.body, req.params.companyId).then(function (companyUpdateRes) {
    res.status(201).send({ status: "success", "payload": companyUpdateRes, message: "Companies saved successfully." })
  }, error => {
    res.status(400).send({ status: "error", "message": error })
  })
}

function createPropertyType(req, res, next) {
  var userId = req.decoded.id;
  var propertyTypeInfo = req.body;

  CompaniesModel.getCompanyIdFromUserId(userId).then(function (companyId) {
    CompaniesModel.createPropertyType(propertyTypeInfo, companyId.id).then(function (companyUpdateRes) {
      res.status(201).send({ status: "success", message: "Property type has been added." })
    }, error => {
      res.status(400).send({ status: "error", "message": error })
    })
  }, error => {
    res.status(400).send({ status: "error", "message": error.message })
  })

}

function getPropertyType(req, res, next) {
  var userId = req.decoded.id;

  CompaniesModel.getCompanyIdFromUserId(userId).then(function (companyId) {
    CompaniesModel.getPropertyType(companyId.id).then(function (companyProperties) {
      res.status(201).send({ status: "success", payload: companyProperties })
    }, error => {
      res.status(400).send({ status: "error", "message": error })
    })
  }, error => {
    res.status(400).send({ status: "error", "message": error.message })
  })

}

module.exports = {
  create,
  update,
  createPropertyType,
  getPropertyType
}