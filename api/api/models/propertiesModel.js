'use strict';
var PropertiesSchemaModel = require('../schema/properties/propertiesSchema');

function create(propertiesData) {
    return new Promise(function (res, rej) {
        var PropertiesModelObj = new PropertiesSchemaModel(propertiesData);
        PropertiesModelObj.save((function (err, data) {
            if (err) return rej(err.message)
            res(data)
        }))
    })
}

function update(propertiesData, propertyId) {
    return new Promise(function (res, rej) {
        PropertiesSchemaModel.findOneAndUpdate({ _id: propertyId }, propertiesData, {
            upsert: true,
            new: true
        }, function (err, property) {
            if (err)
                reject(err);
            resolve(property);
        });
    })
}

module.exports = {
    create,
    update
}