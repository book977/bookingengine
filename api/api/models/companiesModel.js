'use strict';
var CompaniesSchemaModel = require('../schema/companies/companiesSchema');

function create(companiesData) {
  return new Promise(function (res, rej) {
    var CompaniesModelObj = new CompaniesSchemaModel(companiesData);
    CompaniesModelObj.save((function (err, data) {
      if (err) return rej(err.message)
      res(data)
    }))
  })
}

function update(companiesData, companyId) {
  return new Promise(function (res, rej) {
    CompaniesSchemaModel.findOneAndUpdate({ _id: companyId }, companiesData, {
      upsert: true,
      new: true,
    }, function (err, company) {
      if (err)
        rej(err);
      res(company);
    });
  })
}

function createPropertyType(propertyTyeInfo, companyId) {
  return new Promise(function (res, rej) {
    CompaniesSchemaModel.findOne({ _id: companyId }, function (err, company) {
      if (err) return rej(err)

      var availableProperties = company.property_type;
      availableProperties.push(propertyTyeInfo[0]);
      company.property_type = availableProperties;
      company.save(function (err, propertyTypeRes) {
        if (err) return rej(err)
        return res(company);
      })
    });
  })
}


function getPropertyType(companyId) {
  return new Promise(function (res, rej) {
    CompaniesSchemaModel.findOne({ _id: companyId }, 'property_type', function (err, companyPropertyTypes) {
      if (err) return rej(err)

      return res(companyPropertyTypes);
    });
  })
}

function getCompanyIdFromUserId(userId) {
  return new Promise(function (res, rej) {
    CompaniesSchemaModel.findOne({ user_id: userId }, function (err, companyInfo) {
      if (err) return rej({ success: false, message: err });
      if (!companyInfo) {
        return rej({ success: false, message: "You dont have any company associated with your account." })
      } else if (companyInfo) {
        return res({ success: true, id: companyInfo._id })
      }
    })
  })
}


module.exports = {
  create,
  update,
  getCompanyIdFromUserId,
  createPropertyType,
  getPropertyType
}