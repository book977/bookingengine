'use strict';
var UserSchemaModel = require('../schema/user/user');
var CompaniesModel = require("../models/companiesModel");
var jwt = require('jsonwebtoken');
var common = require('../config/common')
var config = common.config();

// Helpers
var RandomKey = require('../helpers/random');

function create(userData) {
    var random = new RandomKey();
    random.generate(function (rand) {
        userData.verification_code = rand;
    })
    userData.created_date = Date.now();

    return new Promise(function (res, rej) {
        var userModelObj = new UserSchemaModel(userData);
        UserSchemaModel.findOne({
            email: userData.email
        }, function (err, user) {
            if (err) return rej(err)

            if (!user) {
                userModelObj.save((function (err, savedUser) {
                    if (err) return rej('Either you have not provided all the information or company already exists.')

                    createCompany({ 'name': userData.company_name, 'user_id': savedUser._id }).then(function (compRes) {
                        getTokenAfterSave({
                            email: userData.email,
                            password: userData.password
                        }, res, rej)
                    }, error => {
                        savedUser.remove({})
                        return rej('Either you have not provided all the information or company already exists.')
                    })

                }))
            } else if (user) {
                return rej('Company already exists.');
            }
        })
    })
}

function createCompany(userCompany) {
    return new Promise(function (res, rej) {
        CompaniesModel.create(userCompany).then(function (companyCreateRes) {
           res(true);
        }, error => {
            rej(false);
        })
    })
}

function getAuth(userReq) {
    return new Promise(function (res, rej) {
        if (userReq.email != undefined && userReq.password != undefined) {
            UserSchemaModel.findOne({
                email: userReq.email
            }, function (err, user) {
                if (err) throw err

                if (!user) {
                    rej('Authentication failed.')
                } else if (user) {
                    user.comparePassword(userReq.password).then(function (userComparePwdRes) {
                        UserSchemaModel.update({ email: userReq.email },
                            { $set: { last_login_date: Date.now() } }, { upsert: true }, function (err, model) {
                                if (err) {
                                    return rej(err)
                                }
                            })
                        var jwtDataForSigning = {
                            id: user._id,
                        }
                        var token = jwt.sign(jwtDataForSigning, config.SUPER_SECRET_FOR_TOKEN, {
                            expiresIn: 2700
                        });
                        var decoded = jwt.decode(token, {
                            complete: true
                        })
                        res({
                            token: token,
                            admin: user.is_admin
                        }

                        )
                    }).catch(function (err) {
                        return rej('Authentication failed. Invalid password.')
                    })
                }
            })
        } else {
            return rej('Data provided is not valid.')
        }
    })
}

var getTokenAfterSave = function (userCred, res, rej) {
    UserSchemaModel.findOne({
        email: userCred.email
    }, function (err, user) {
        if (err) throw err

        if (!user) {
            rej('Authentication failed.')
        } else if (user) {

            user.comparePassword(userCred.password).then(function (userComparePwdRes) {
                var jwtDataForSigning = {
                    id: user._id,
                }
                var token = jwt.sign(jwtDataForSigning, config.DBENV.SECRET, {
                    expiresIn: 2700
                });
                var decoded = jwt.decode(token, {
                    complete: true
                })
                res({
                    token: token
                })
            }).catch(function (err) {
                rej(err)
            })
        }
    })
}


module.exports = {
    create,
    getAuth
}