'use strict';
var User = require("../../models/user");
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CompaniesSchema = new Schema({
  user_id: {type:Schema.Types.ObjectId, ref:'User', required:true},
  name: { type: String, required: true },
  description: { type: String, required: false },
  address: { type: String, required: false },
  resources: [{
    name: { type: String, required: false },
    desc: { type: String, required: false },
    extra_price: { type: String, required: false },
    MaxQty: { type: String },
  }],
  property_type: [{
    name: { type: String, required: false },
    address: { type: String, required: false },
    price: { type: String, required: false },
    is_cancelable: { type: Boolean, default: false },
    max_no_of_guest: { type: Number, required: false }
  }],
  delegated_properties: [{
    property_id: { type: Number, required: false }
  }],
  websiteUrl: { type: String },
  authenticationKey: { type: String }
});


module.exports = mongoose.model('Companies', CompaniesSchema);