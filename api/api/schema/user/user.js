'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt'), SALT_WORK_FACTOR = 10;

var UserSchema = new Schema({
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    company_name: { type: String, required: true, unique: true },
    phone: { type: String, required: false },
    email_verified: { type: Boolean, required: true, default: false },
    verification_code: { type: String, required: true },
    is_admin: { type: Boolean, default: true },
    contact: {
        country: { type: String, required: false },
        city: { type: String, required: false },
        state: { type: String, required: false },
        zip: { type: String, required: false },
        address: { type: String, required:false }
    },
    reset_password_token: { type: String },
    reset_password_token_expiration: { type: Date, default: null },
    created_date: { type: Date },
    updated_date: { type: Date, default: Date.now() },
    last_login_date: { type: Date }
});


/**
 * hashes password for save
 * 
 */
UserSchema.pre('save', function (next) {
    if (this.isNew) {
        var currentDate = new Date();
        this.updated_on = '';

        if (!this.created_date) {
            this.created_date = currentDate;
            this.last_login_date = currentDate;
        }
    }

    var user = this;

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
        if (err) return next(err);

        // hash the password using our new salt
        bcrypt.hash(user.password, SALT_WORK_FACTOR, function (err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.password = hash;
            next();
        });
    });
});

UserSchema.methods.getHashedPwd = function (pwd) {
    return new Promise(function (res, rej) {
        // hash the password using our new salt
        bcrypt.hash(pwd, SALT_WORK_FACTOR, function (err, hash) {
            if (err) return rej(err);

            res(hash)
        });
    })

};

UserSchema.methods.comparePassword = function (candidatePassword) {
    var UserObj = this;
    return new Promise(function (res, rej) {
        bcrypt.compare(candidatePassword, UserObj.password, function (err, isMatch) {
            if (isMatch) return res(true)
            rej(false);
        });
    })

};

module.exports = mongoose.model('User', UserSchema);