'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PropertiesSchema = new Schema({
    _CID: { type: Number, required: true, unique: true },
    company: {
        _CID: { type: String, required: true },
        name: { type: String, required: true },
        address: { type: String, required: true }
    },
    property_type: {
        name: { type: String, required: true },
        address: { type: String, required: true },
        price: { type: String, required: true },
        is_cancelable: { type: Boolean, default: true },
        max_no_of_guest: { type: Number, required: true }
    },
    reserved_date_time: { type: Date, required: true },
});



module.exports = mongoose.model('Properties', PropertiesSchema);